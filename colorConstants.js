/*
* All constant and default values for the visual chip and simulated TV
* Makes changing color schemes very easy
*/

//Background layer colors
var backgroundFill = '#000000';
var backgroundStroke = 'rgba(255,255,255,0.5)';

var colors = ['#303030','#00A0A0','rgba(0,0,80,0.75)','#E0E000'];
var transistorColor = 'rgba(100,255,0,0.8)';
//Which layers do we draw? (defaults)
var drawlayers = [true, true, true,true];
var drawTLayer = true;
var backgroundDrawOrder = [0,1,-1,2,3];

//The overlay color is drawn over powered nodes
var overlayColor = 'rgba(255,255,255,0.2)';

var poweredHilite = 'rgba(255,0,0,0.7)';
var unpoweredHilite = 'rgba(255,255,255,0.7)';
var transistorHiliteStroke = 'rgba(255,255,255,0.7)';//Unused currently
//Color of an input transistor (A transistor which can change the nodes state)
var inputTransistorColor = '#FF00FF';
//Color of an output transistor (A transistor which is powered by the node)
var outputTransistorColor = '#FFFF00';
var transistorPoweredHilite = 'rgba(255,255,255,0.4)';

//Box label font and colors
var labelFont = 'px sans-serif';
var boxFill = '#fff';
var boxStroke = '#fff';
var labelStroke = '#000';

//TV-output related colors
var tvBackgroundColor = '#FFFFFF';
var scanColor = '#FFFFFF';
var undoColor = 'rgba(0,0,0,0.1)';
var emptyColor = '#C0C0C0';
var syncColor = '#FF0000';
var leftPlayerColor = '#0000FF';
var rightPlayerColor = '#00FF00';
var ballColor = '#FFFF00';
var fieldColor = "#000000";