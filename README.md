This is the JavaScript simulator for the AY-3-8500-1 PONG-like chip

It can (currently) simulate gamefield drawing and game selection

Based on the work of the visual6502 team (Their source is at https://github.com/trebonian/visual6502)
You can find more info at my blog (https://nerdstuffbycole.blogspot.com/)
Also thanks to Sean Riddle for the die photos and
David Winter of http://www.pong-story.com/ for preservation of datasheets and other information about the chip

Note the various licenses and Copyright associated with each file.
Unless otherwise noted, all code is under the MIT License

While you are not legally required to, it would be appreciated that, if you reproduce or develop this work, you;

	-Provide links to the original creators (Cole Johnson and the Visual6502 team) beyond those in the source
	-Notify me of any projects/uses the simulator has helped you with
	-Publish the source of any new versions so others can learn from and expand upon our work

Enjoy! -Cole Johnson

"Enjoy!" -The Visual6502 Team
