/*
 Copyright (c) 2018 Cole Johnson

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

var nodenames ={
//Critical nodes
gnd: 0,      // pads: ground
vcc: 19,       // pads: power
reset: 920,   // pads: reset
clk: 563,	//clock pin

//Miscellaneous nodes
disable: 8,
internal_clock: 17,
half_clock: 446,
internal_reset: 141,
reset_latch: 876,

//all pins besides the 4 critical ones
pinLPin: 22,//left player in
pinRPin: 7,//right player in
pinLPout: 1,//left player out
pinRPout: 2,//right player in
pinManualServe: 3,//manual serve
pinBallSpeed: 4,//ball speed
pinBallOut: 5,//ball out
pinBallAngle: 34,//ball angle
pinSound: 435,//sound out
pinHitIn: 720,//hit in
pinShotIn: 854,//shot in
pinSFout: 926,//score/field out
pinPractice: 922,//practice select
pinSquash: 923,//squash select
pinSoccer: 921,//soccer select
pinTennis: 924,//tennis select
pinRifle2: 832,//rifle game 2
pinRifle1: 694,//rifle game 1
pinSyncOut: 344,//sync out
pinBatSize: 249,//bat size

//Sync circuitry related
to_sync: 337,
hsyncNOT_ON2: 98,
hsyncON: 254,
hsyncNOT_ON: 248,
vsyncON: 21,
vsyncNOT_ON: 205,

//Shift register related
hrz_shifter: 414,
vrt_shifter: 294,
hrz_reset: 388,
vrt_reset: 390,
hrz_feeder: 359,
vrt_feeder: 592,

//Paddle related
rightplayer: 15,
leftplayer: 18,
NOT_LPhrz: 197,
NOT_RPhrz: 210,
aboveLP: 211,
aboveRP: 48,
belowLP: 177,
belowRP: 159,

//Individual paddles
LPgoal: 727,
RPforward: 704,
LPforward: 700,//and squash
RPsquash: 702,
RPgoal: 732,

//Not yet organized nodes
topBottomBounce: 130,
nonRiflegameSound: 745,

//Score-decoding related
score0: 511,
score1: 509,
score2: 507,
score3: 505,
score4: 503,
score5: 501,
score6: 576,
score7: 577,
score8: 578,
score9: 587,
NOT_draw1: 516,

//Score drawing related
drawTop: 600,
drawUpper: 619,
drawMiddle: 609,
drawLower: 627,
drawBottom: 604,

segN: 530,
segNE: 527,
segSE: 526,
segS: 531,
segSW: 532,
segNW: 534,
segMiddle: 529,
segNECorner: 528,
segNWCorner: 535,
segMiddleWest: 533,

NOT_drawLeft: 658,
NOT_drawMiddle: 657,
NOT_drawRight: 654,

scoreOut: 729,
scoreDrawShift1: 676,
scoreDrawShift2: 693,
scoreDrawReset: 687,

//Internal game selection
rifle1: 801,
rifle2: 762,
tennis: 919,
soccer: 900,
squash: 730,
practice: 789,
NOT_soccer: 896,

practiceSoccer: 23,
r2andSoccer: 28,
tennisUnknownMode: 813,
rifle1UnknownMode: 6,

//Game control related
closedGoals: 825,
disableP1Squash: 812,
disableP2Squash: 765,
isRifleGame: 157,
disableP2Forward: 742,
leftWallDisable: 751,
disableGoalPaddles: 783,//and middle wall

sidelines: 881,
middleWall: 796,
leftWall: 748,
rightWall: 755,
fieldOut: 894,

insideGoal: 741,

//Ball related
NOT_ballinbox: 31,
NOT_ballhit: 33,
ball: 30,
ballXinvalid: 95,
ballYinvalid: 35,

//score adjustment nodes
//left (on-chip) counter controls RIGHT (on-screen) digits &vice-versa
//0=topmost flip flop=LSB
scrLtop: 477,
scrRtop: 482,
rightInc: 475,

scrL0u: 478,
scrL1u: 545,
scrL2u: 582,
scrL3u: 621,
scrR0u: 481,
scrR1u: 548,
scrR2u: 585,
scrR3u: 624,

scrL0l: 493,
scrL1l: 568,
scrL2l: 594,
scrL3l: 651,
scrR0l: 494,
scrR1l: 569,
scrR2l: 593,
scrR3l: 652,


//These are the horizontal/vertical control signals, starting from the top
hrzControl1: 445,
hrzControl2: 453,
hrzControl3: 456,
hrzControl4: 462,
hrzControl5: 464,
hrzControl6: 468,
hrzControl7: 474,
hrzControl8: 486,
hrzControl9: 487,
hrzControl10: 491,
hrzControl11: 492,
hrzControl12: 495,
hrzControl13: 293,
hrzControl14: 305,
NOT_hrzControl1: 437,
NOT_hrzControl6: 471,
NOT_hrzControl12: 496,

vrtControl1: 308,
vrtControl2: 301,
vrtControl3: 559,
vrtControl4: 566,
vrtControl5: 571,
vrtControl6: 575,
vrtControl7: 519,
vrtControl8: 555,
vrtControl9: 465,
vrtControl10: 466,
NOT_inVrtRange: 198,
everyOtherScanline: 595,
}
